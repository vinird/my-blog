# Rsyslog, remote loggin configuration on Debian 8 server

The first step is install rsyslog, in most cases it is already installed.

    sudo apt-get install rsyslog

Rsyslog have to be installed on both, the client and the server.

## Configure rsyslog server

The server monitor configuration is quite simple. You need to configure the **/etc/rsyslog.conf** file.

### Allow TCP connections

In the first section of the file you will find something like this:

    #################  
    #### MODULES ####  
    #################  


    $ModLoad imuxsock # provides support for local system logging  
    $ModLoad imklog   # provides kernel logging support  
    #$ModLoad immark  # provides --MARK-- message capability  

    # provides UDP syslog reception  
    #$ModLoad imudp  
    #$UDPServerRun 514  

    # provides TCP syslog reception  
    #$ModLoad imtcp  
    #$InputTCPServerRun 514  


    ###########################  
    #### GLOBAL DIRECTIVES ####  
    ###########################  

There are two important lines that we need to edit:  

```#$ModLoad imtcp```  

```#$InputTCPServerRun 514```  

What we need to do here is uncomment, that way we can get **tcp** connections. The file should looks like this:

    ################  
    #### MODULES ####  
    #################  
    
    
    $ModLoad imuxsock # provides support for local system logging  
    $ModLoad imklog   # provides kernel logging support  
    #$ModLoad immark  # provides --MARK-- message capability  
    
    # provides UDP syslog reception  
    #$ModLoad imudp  
    #$UDPServerRun 514  
    
    # provides TCP syslog reception  
    $ModLoad imtcp  
    $InputTCPServerRun 514  
    
    
    ###########################  
    #### GLOBAL DIRECTIVES ####  
    ###########################  

### Allow clients to connect to the server

Now we have to tell to the service rsyslog which are the allow host. In the same file **/etc/rsyslog.conf**

    # loadbalancing_1

    *.*  @132.232.12.92

In the example above we are define a connection to an especific ip address.

If we want to use a custom path for the logs output we have to add the follow code before the declaration of the clients ip:

    # Log each remote host into it's own directory and then discard remote server logs:
    $template RemoteHost,
    "/var/custom_path/logs/remote-hosts/%HOSTNAME%/%HOSTNAME%-%$YEAR%%$MONTH%%$DAY%.log"
    if ($hostname != '<remotehost>') then ?RemoteHost
    & ~

The last step is to restart the service.

    service rsyslog restart

## Configure rsyslog clients

### Allow TCP connections

For the clients the configuration is pretty similar, first we need to allow tcp connections

The original file (**/etc/rsyslog**) looks like the follow:

    #################  
    #### MODULES ####  
    #################  


    $ModLoad imuxsock # provides support for local system logging  
    $ModLoad imklog   # provides kernel logging support  
    #$ModLoad immark  # provides --MARK-- message capability  

    # provides UDP syslog reception  
    #$ModLoad imudp  
    #$UDPServerRun 514  

    # provides TCP syslog reception  
    #$ModLoad imtcp  
    #$InputTCPServerRun 514  


    ###########################  
    #### GLOBAL DIRECTIVES ####  
    ###########################  

Once we have edited it should looks like this:

    #################
    #### MODULES ####
    #################

    $ModLoad imuxsock # provides support for local system logging
    $ModLoad imklog   # provides kernel logging support
    #$ModLoad immark  # provides --MARK-- message capability

    # provides UDP syslog reception
    #$ModLoad imudp
    #$UDPServerRun 514

    # provides TCP syslog reception
    $ModLoad imtcp
    $InputTCPServerRun 514


    ###########################
    #### GLOBAL DIRECTIVES ####
    ###########################

### Add the remote host address

After that we have to tell to the client which is the server address. To do this we need to include the next code in the bottom of the file **/etc/rsyslog.conf**

    # Custom
    # remote host is: name/ip:port, e.g. 192.168.0.1:514, port optional
    #*.* @remote-host:514
    *.*         @@remote-host:514

Finally we have to restart the service

    service rsyslog restart

And that`s it.