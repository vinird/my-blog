# Install Nagios Monitor

[Reference link](https://www.linode.com/docs/uptime/monitoring/install-nagios-4-on-ubuntu-debian-8)

Update repository and packages.

	sudo apt-get update && sudo apt-get upgrade

**To install Nagios monitoring system you must need to set up a web server with a database. In a Debian 8 server you need to run the follow list of commands.. [Reference link](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-debian)**

Add new user and group.

	sudo useradd nagios_user
	sudo groupadd nagcmd
	sudo usermod -a -G nagcmd nagios && sudo usermod -a -G nagcmd www-data

Install Nagios dependencies.

	sudo apt-get install build-essential unzip openssl libssl-dev libgd2-xpm-dev xinetd apache2-utils

Download and uncompress Nagios.

	wget https://assets.nagios.com/downloads/nagioscore/releases/nagios-4.1.1.tar.gz

	tar -xvf nagios-4.*.tar.gz

	cd nagios-4.*

Compile Nagios.

	./configure --with-nagios-group=nagios --with-command-group=nagcmd

	make all

Configure Apache (virtual hosts).

	sudo a2enmod rewrite && sudo a2enmod cgi

	sudo cp sample-config/httpd.conf /etc/apache2/sites-available/nagios4.conf

	sudo chmod 644 /etc/apache2/sites-available/nagios4.conf

	sudo a2ensite nagios4.conf

Create a user for Nagios.

	sudo htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin

Restart Apache.

	sudo service apache2 restart

## Install Nagios plugins.

	wget http://www.nagios-plugins.org/download/nagios-plugins-2.1.1.tar.gz

	tar -xvf nagios-plugins-2*.tar.gz

	cd nagios-plugins-2.*

	./configure --with-nagios-user=nagios?user --with-nagios-group=nagios --with-openssl

	make

	sudo make install

Start Nagios.

	sudo service nagios start

	http://hostip/nagios

