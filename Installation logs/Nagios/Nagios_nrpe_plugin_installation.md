## Nagios plugin NRPE for monitoring (client)
[Link to download the reference document](https://github.com/NagiosEnterprises/nrpe/blob/master/docs/NRPE.pdf)

	apt-get install xinetd

	apt-get install make

	mkdir ~/downloads
	
	cd downloads
### Install nagios plugins
	wget https://nagios-plugins.org/download/nagios-plugins-2.2.1.tar.gz#_ga=2.30406329.701078528.1498020220-281424521.1496848134

	tar xzf nagios-plugins-2.2.1.tar.gz

	cd nagios-plugins-2.2.1

	./configure

	make

	make install

	useradd nagios

	groupadd nagios

	usermod -a -G nagios nagios

	chown nagios.nagios /usr/local/nagios

	chown -R nagios.nagios /usr/local/nagios/libexec

	cd ..
### Install nrpe plugin
	wget https://github.com/NagiosEnterprises/nrpe/releases/download/nrpe-3.1.0/nrpe-3.

	1.0.tar.gz

	tar xzf nrpe-3.1.0.tar.gz

	cd nrpe-nrpe-3.0

	./configure

		> *** Configuration summary for nrpe 3.1.0-rc1 2017-04-06 ***:
		> General Options:  
 		> -------------------------  
		> NRPE port:    5666  
		> NRPE user:    nagios  
		> NRPE group:   nagios  
		> Nagios user:  nagios  
		> Nagios group: nagios  

Review the options above for accuracy.  If they look okay, type 'make all' to compile the NRPE daemon and client or type 'make' to get a list of make options.

	make all

	make install

	make install-config

	make install-inetd

>***** MAKE SURE 'nrpe 5666/tcp' IS IN YOUR /etc/services FILE

	make install-init

	service xinetd restart

	systemctl reload xinetd

	systemctl enable nrpe.service && systemctl start nrpe.service

	netstat -at | egrep "nrpe|5666"

		> tcp 0 0 *:nrpe *:* LISTEN

Allow access for the Nagios server.

	nano /usr/local/nagios/etc/nrpe.cfg

Then add the follow line with your monitor server address.
	
	allowed_hosts=x.x.x.x

If everything worked, add the hostname or IP address of the nagios server to the /etc/xinetd.d/nrpe file, or /etc/hosts-allow and hosts-deny.
 
To add any command you need to include them in: **/usr/local/nagios/etc/nrpe.cfg**

	command[check_users]=/usr/local/nagios/libexec/check_users -w 5 -c 10  
	command[check_load]=/usr/local/nagios/libexec/check_load -r -w .15,.10,.05 -c .30,.25,.20  
	command[check_hda1]=/usr/local/nagios/libexec/check_disk -w 20% -c 10% -p /dev/vda1  
	command[check_zombie_procs]=/usr/local/nagios/libexec/check_procs -w 5 -c 10 -s Z  
	command[check_total_procs]=/usr/local/nagios/libexec/check_procs -w 150 -c 200  

### Custom commands

	command[check_http]=/usr/local/nagios/libexec/check_http -H google.com  
	command[check_ping]=/usr/local/nagios/libexec/check_ping -H 8.8.8.8 -w 40,2% -c 60,5%  
	command[check_uptime]=/usr/local/nagios/libexec/check_uptime  
	command[check_tcp]=/usr/local/nagios/libexec/check_tcp -H localhost -p 5666  

After any configuration you have to reload or restart xinetd and nrpe.

	systemctl reload xinetd.service && service nrpe reload

## Nagios plugin NRPE (Monitor server)

[Link to the reference document](https://github.com/NagiosEnterprises/nrpe/blob/master/docs/NRPE.pdf)

	wget https://github.com/NagiosEnterprises/nrpe/releases/download/nrpe-3.1.0/nrpe-3.1.0.tar.gz

	tar xzf nrpe-3.1.0.tar.gz

	cd nrpe-3.1.0/

	./configure

	make check_nrpe
		
		>General Options:  
		> -------------------------   
		> NRPE port:    5666   
		> NRPE user:    nagios  
		> NRPE group:   nagios  
		> Nagios user:  nagios  
		> Nagios group: nagios   



Review the options above for accuracy.  If they look okay, type 'make all' to compile the NRPE daemon and client or type 'make' to get a list of make options.
	
	make install-plugin
 
Each new configuration file must be declared in: **usr/local/nagios/etc/nagios.cfg**

	/usr/local/nagios/libexec/check_nrpe -H 192.168.0.1

		> NRPE v3.0

	vimacs /usr/local/nagios/etc/commands.cfg

Then add the command.
 
	define command{  
		command_name     	check_nrpe  
		command_line		$USER1$/check_nrpe -H $HOSTADDRESS$ -c $ARG1$  
	}
 
You must create three files with the follow code, this should be placed in: **/usr/local/nagios/etc/objects/**

	define host{  
		name						linux-box ; Name of this template  
		use							generic-host ; Inherit default values  
		check_period				24x7  
		check_interval				5  
		retry_interval				1  
		max_check_attempts			10  
		check_command				check-host-alive  
		notification_period			24x7   
		notification_interval		30  
		notification_options		d,r  
		contact_groups				admins  
		register					0  
	}  	

	define host{  
		use							linux-box  
		host_name					remotehost  
		alias						alias of the server  
		addres						serverIP  
	}  

	define service{  
		use							generic-service  
		host_name					remotehost  
		service_description			check_command  
		CPU Load					check_nrpe!check_load  
	}

You can define more services, but they must be declare on the client in the file as a command: **/usr/local/nagios/etc/nrpe.cfg**

	service nagios restart
